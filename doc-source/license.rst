=========
License
=========

``ytools3`` is licensed under the :choosealicense:`Apache-2.0`

.. license-info:: Apache-2.0

.. license::
	:py: ytools3
