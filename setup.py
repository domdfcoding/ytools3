#!/usr/bin/env python
# This file is managed by 'repo_helper'. Don't edit it directly.

# stdlib
import pathlib
import shutil
import sys

# 3rd party
from setuptools import setup

sys.path.append('.')

extras_require = {}

repo_root = pathlib.Path(__file__).parent
install_requires = (repo_root / "requirements.txt").read_text(encoding="UTF-8").split('\n')

setup(
		description=
		"Library for validating yaml files against schema and selectively dumping nodes from yaml (or json) documents in yaml or json format.",
		extras_require=extras_require,
		install_requires=install_requires,
		name="ytools3",
		py_modules=[],
		)

shutil.rmtree("ytools3.egg-info", ignore_errors=True)
